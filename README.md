# README #

### ABOUT ###

* This is a really simple tool to concatenate and minify .js using [grunt-contrib-concat](https://github.com/gruntjs/grunt-contrib-concat) and [grunt-contrib-uglify
](https://github.com/gruntjs/grunt-contrib-uglify)

### USAGE ###

* Clone this repo.
* Open a command line window.
* Install [Grunt](http://gruntjs.com/getting-started): `npm install -g grunt-cli`.
* Navigate to the directory of this repo.
* Run `npm install` to install dependencies.
* You're ready to go. Place the JS to be concat/minified in `src`.
* Run command `grunt min` to run the task.
* On completion, minified JS is found at `dist/script.min.js`.